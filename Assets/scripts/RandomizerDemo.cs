﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomizerDemo : MonoBehaviour
{


    public Text first; // text fields for nine combinations
    public Text second;
    public Text third;
    public Text forth;
    public Text fifth;
    public Text sixth;
    public Text seventh;
    public Text eighth;
    public Text nineth;
    public Text noOfTries;
    public Text messageHolder;
    public GameManager newGameManager;
    int keyValue = 0;
    int tries = 0;
    int totalTries = 5;




    private List<string> konamiKeyCode = new List<string>();      // list to store the keycode name
    public List<string> randomList = new List<string>(); //random list to store random combinations

    private void Start()
    {

        konamiKeyCode.Add("up");
        konamiKeyCode.Add("down");
        konamiKeyCode.Add("right");
        konamiKeyCode.Add("left");
        konamiKeyCode.Add("b");
        konamiKeyCode.Add("a");

        for (int count = 0; count <= 8; count++)
        {
            randomList.Add(konamiKeyCode[Random.Range(0, 5)]);
        }       
    }
    private void Update()
    {
              
        if (Input.GetKeyDown(randomList[0]))
        {


            if (keyValue == 0)
            {
                first.text = randomList[0];
                messageHolder.text = ("Your first guess is correct.Choose your second  guess");

            }
            if (keyValue == 1)
            {
                second.text = randomList[0];
                messageHolder.text = ("Your second guess is correct.Choose your third  guess");
            }
            if (keyValue == 2)
            {
                third.text = randomList[0];
                messageHolder.text = ("Your third guess is correct.Choose your forth  guess");
            }
            if (keyValue == 3)
            {
                forth.text = randomList[0];
                messageHolder.text = ("Your forth guess is correct.Choose your fifth  guess");
            }
            if (keyValue == 4)
            {
                fifth.text = randomList[0];
                messageHolder.text = ("Your fifth guess is correct.Choose your sixth  guess");
            }
            if (keyValue == 5)
            {
                sixth.text = randomList[0];
                messageHolder.text = ("Your sixth guess is correct.Choose your seventh  guess");
            }
            if (keyValue == 6)
            {
                seventh.text = randomList[0];
                messageHolder.text = ("Your seventh guess is correct.Choose your eighth  guess");
            }
            if (keyValue == 7)
            {
                eighth.text = randomList[0];
                messageHolder.text = ("Your eighth guess is correct.Choose your final guess to win the game");
            }
            if (keyValue == 8)
            {
                nineth.text = randomList[0];
                
            }


            randomList.RemoveAt(0);
            keyValue++;
        }

        else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.B))
        {

          
                int remainingTries = totalTries - tries;
                noOfTries.text = remainingTries.ToString();
                messageHolder.text = ("Wrong guess, try other guess");

                tries++;         
        }

        if (tries >= totalTries)
        {
            newGameManager.GameOverLost();
            return;
        }


        if (randomList.Count < 1)
        {

            newGameManager.GameWin();
            return;
        }

    }
}

